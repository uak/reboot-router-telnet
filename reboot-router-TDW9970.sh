#!/bin/bash

## Script to reboot TP-link router using telnet
## Works with TD-W9970
## It records current ip, login to the router, initiate restart command, then monitors for internet and report the old and new ip then play a sound.

#alternative get ip server
#dig TXT +short o-o.myaddr.l.google.com @ns1.google.com
#dig +short myip.opendns.com @resolver2.opendns.com

# Version 0.1

user_name="admin"
password="password"
router_ip="192.168.0.1"

# website to test ping with
ping_web_site="www.google.com"

#sound file
sound_file="/usr/share/sounds/freedesktop/stereo/network-connectivity-established.oga"

# Get my current IP
old_ip=$(dig +short myip.opendns.com @resolver1.opendns.com)

#connect to router and restart
eval "{ echo $user_name; sleep 1; echo $password ; sleep 1; echo 'dev reboot'; sleep 5; }" | telnet $router_ip 

#wait for connectivity and report new IP
until ping -c1 $ping_web_site >/dev/null 2>&1; do :; done

#Get new IP
new_ip=$(dig +short myip.opendns.com @resolver1.opendns.com)

#Report old and new IP
echo "Old IP: $old_ip"
echo "New IP: $new_ip"

#Play a sound when finished
paplay $sound_file 
